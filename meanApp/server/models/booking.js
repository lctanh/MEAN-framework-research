const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const bookingSchema = new Schema({
    mailable: String,
    email: String,
    type: String,
    destination: String,
    departure: String,
    return: String
});

module.exports = mongoose.model('booking', bookingSchema, 'booking');