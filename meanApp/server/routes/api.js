const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Booking  = require('../models/booking');

const db = "mongodb://lctanh:123456789@ds135522.mlab.com:35522/mydb";
mongoose.Promise = global.Promise;
mongoose.connect(db, function(err){
    if(err){
        console.log('Error! ' + err);
    }
});

router.get('/booklist', function(req, res){
    console.log('Get request for all booking list in DB');
    Booking.find({})
    .exec(function(err, booklist){
        if(err){
            console.log('Error retrieving booking list');
        } else {
            res.json(booklist);
        }
    });
});

router.get('/booklist/:id', function(req, res){
    console.log("Get request for a single book list");
    Booking.findById(req.params.id)
    .exec(function(err, booklist){
        if(err){
            console.log("Error retrieving book list");
        }else {
            res.json(booklist);
        }
    });
});

router.post('/addbook', function(req, res){
    console.log('Add a ticket into booking list');
    var newTicket = new Booking();
    newTicket.mailable = req.body.mailable;
    newTicket.email = req.body.email;
    newTicket.type = req.body.type;
    newTicket.destination = req.body.destination;
    newTicket.departure = req.body.departure;
    newTicket.return = req.body.return;
    
    newTicket.save(function(err, insertedTicket){
        if(err) {
            console.log('Error saveing new ticket' + err);
        } else {
            res.json(insertedTicket);
        }
    });
});

router.put('/booklist/:id', function(req, res){
    console.log('Update a ticket into booking list');
    Booking.findByIdAndUpdate(req.params.id, {
        $set: {mailable: req.body.mailable,
            email:  req.body.email,
            type: req.body.type,
            destination: req.body.destination,
            departure: req.body.departure,
            return: req.body.return}
    },
    {
        new: true
    },
    function(err, updatedTicket){
        if(err){
            res.send('Error updating ticket' + err)
        } else {
            res.json(updatedTicket);
        }
    });
});

router.delete('/booklist/:id', function(req, res){
    console.log('Deleting a ticket into booking list');
    Booking.findByIdAndRemove(req.params.id, function(err, deletedTicket){
        if(err){
            res.send('Error deleting ticket');
        } else {
            res.json(deletedTicket);
        }
    });
});

module.exports = router;